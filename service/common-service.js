
// List of states:
// State list of Angular module
// State list of JQuery module
// State list of Bootstrap module
// State list of Software module
// State list of Home module

(function () {
    'use strict';
    angular.module('webspace').service('commonService',function (){
            
        this.displaySize = function () {
            var w = window.innerWidth,
                h = window.innerHeight,
                size = 'Unknown';

            if (w >= 1200) {
                size = 'Extra large';
            } else if (w >= 992) {
                size = 'Large';
            } else if (w >= 768) {
                size = 'Medium';
            } else if (w >= 576) {
                size = 'Small';
            } else {
                size = 'Extra small';
            }
            return w+'x'+h+' > '+size;
        }

        this.firebaseConfig = function () {
            var config = {
                apiKey: "AIzaSyDSokFtVp2xwl-aEgR_WIKlamal2RCuY0Y",
                authDomain: "webspace-15fdd.firebaseapp.com",
                databaseURL: "https://webspace-15fdd.firebaseio.com",
                storageBucket: "webspace-15fdd.appspot.com",
                messagingSenderId: "792258188769"
            };

            return config;
        }

        this.getPath = function () {
            var pathList = ({
                'debug': false, // For debugging 'true/false'
                'events': false, // For Event 'true/false'
                'modules': [{
                        name : 'downloadjs',
                        files: ['js/strictly-jquery/download.min.js']
                    },{
                        name : 'gifplayer', 
                        files: ['js/strictly-jquery/jquery.gifplayer.js', 'css/complex/gifplayer.css']
                    },{
                        name : 'highlight', 
                        files: ['js/strictly-jquery/highlight/highlight.pack.js', 'js/strictly-jquery/highlight/highlightjs-line-numbers.min.js', 'css/complex/highlight-vs.css']
                    },{
                        name : 'ng-scrollbars', 
                        files: ['js/complex/scrollbars/jquery.mCustomScrollbar.concat.min.js', 'js/complex/scrollbars/scrollbars.min.js', 'css/complex/jquery.mCustomScrollbar.min.css']
                    },{
                        name : 'swangular', 
                        files: ['js/complex/swangular/sweetalert2.min.js', 'js/complex/swangular/swangular.js', 'css/complex/sweetalert2.min.css']
                    },{
                        name : 'angucomplete-alt', 
                        files: ['js/strictly-angular/angucomplete-alt.min.js', 'css/complex/angucomplete.css']
                    },{
                        name : 'summernote', 
                        files: ['js/complex/summernote/summernote.js', 'js/complex/summernote/angular-summernote.min.js', 'css/complex/summernote.css']
                    },{
                        name : 'hotkeys', 
                        files: ['js/strictly-angular/hotkeys.min.js', 'css/complex/hotkeys.min.css']
                    },{
                        name : 'ng-tagsinput', 
                        files: ['js/complex/tagsinput/lodash.core.js', 'js/complex/tagsinput/ng-tags-input.min.js', 'css/complex/ng-tags-input.css']
                    },{
                        name : 'gridster', 
                        files: ['js/complex/gridster/angular-gridster.min.js', 'js/complex/gridster/jquery.resize.js', 'css/complex/angular-gridster.css']
                    },{
                        name : 'ui-cropper', 
                        files: ['js/strictly-angular/ui-cropper.js', 'css/complex/ui-cropper.css']
                    },{
                        name : 'ws-table', 
                        files: ['js/strictly-angular/dirPagination.js']
                    },{
                        name : 'colorpicker', 
                        files: ['js/strictly-angular/bootstrap-colorpicker-module.min.js', 'css/complex/colorpicker.min.css']
                    },{
                        name : 'typewrite', 
                        files: ['js/strictly-angular/angular-typewrite.js', 'css/complex/angular-typewrite.css']
                    },{
                        name : 'bxsliderjs', 
                        files: ['js/strictly-jquery/jquery.bxslider.min.js', 'css/complex/jquery.bxslider.css']
                    },{
                        name : 'chartjs', 
                        files: ['js/strictly-jquery/Chart.bundle.min.js']
                    },{
                        name : 'camanjs', 
                        files: ['js/strictly-jquery/caman.full.min.js', 'css/complex/caman-range.css']
                    },{
                        name : 'tiltjs', 
                        files: ['js/strictly-jquery/tilt.jquery.min.js', 'css/complex/tiltjs.css']
                    },{
                        name : 'slider-pips', 
                        files: ['js/complex/slider-pips/jquery-ui.min.js', 'js/complex/slider-pips/jquery-ui-slider-pips.min.js', 'css/complex/slider-pips/jquery-ui.css', 'css/complex/slider-pips/jquery-ui-slider-pips.min.css']
                    },{
                        name : 'defiantjs', 
                        files: ['js/strictly-jquery/defiant.min.js', 'https://www.gstatic.com/firebasejs/3.6.6/firebase.js']
                    },{
                        name : 'html2canvas', 
                        files: ['js/strictly-jquery/html2canvas.min.js', 'js/strictly-jquery/jspdf.min.js']
                    },{
                        name : 'isotope', 
                        files: ['js/strictly-jquery/isotope.pkgd.min.js', 'css/complex/isotope.css']
                    },{
                        name : 'jssocials', 
                        files: ['js/strictly-jquery/jssocials.min.js', 'css/complex/jssocials/jssocials.css', 'css/complex/jssocials/jssocials-theme-flat.css']
                    },{
                        name : 'firebase', 
                        files: ['https://www.gstatic.com/firebasejs/3.6.6/firebase.js']
                    },{
                        name : 'module-angular', 
                        files: ['pages/module-angular.js']
                    },{
                        name : 'module-jquery', 
                        files: ['pages/module-jquery.js']
                    },{
                        name : 'module-bootstrap', 
                        files: ['pages/module-bootstrap.js']
                    },{
                        name : 'module-software', 
                        files: ['pages/module-software.js']
                    },{
                        name : 'module-home', 
                        files: ['pages/module-home.js']
                    }]
            });
            return pathList;
        }

        this.getState = function () {
            var states = [{
                // State list of Angular module
                name: 'angular', 
                url: '/angular', 
                views:{
                    "content": {
                        templateUrl: 'pages/angular/angular.html'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('module-angular');
                    }]
                }                
                },{
                name: 'angular.ng-scrollbars', 
                url: '/ng-scrollbars',
                views:{
                    "content@": {
                        templateUrl: 'pages/angular/ng-scrollbars.html',
                        controller: 'scrollbarsController as scrollbars'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('ng-scrollbars',{serie:true});
                    }]
                }
                },{ 
                name: 'angular.swangular', 
                url: '/swangular',
                views:{
                    "content@": {
                        templateUrl: 'pages/angular/swangular.html',
                        controller: 'swangularController as swangular'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load(['swangular', 'gifplayer'], {serie: true});
                    }]
                }
                },{ 
                name: 'angular.angucomplete-alt', 
                url: '/angucomplete-alt',
                views:{
                    "content@": {
                        templateUrl: 'pages/angular/angucomplete-alt.html',
                        controller: 'angucompleteController as angucomplete'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('angucomplete-alt',{serie: true});
                    }]
                }
                },{ 
                name: 'angular.summernote', 
                url: '/summernote',
                views:{
                    "content@": {
                        templateUrl: 'pages/angular/summernote.html',
                        controller: 'summernoteController as summernotejs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('summernote',{serie: true});
                    }]
                }
                },{ 
                name: 'angular.hotkeys', 
                url: '/hotkeys',
                views:{
                    "content@": {
                        templateUrl: 'pages/angular/hotkeys.html',
                        controller: 'hotkeysController as hotkeysjs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('hotkeys',{serie: true});
                    }]
                }
                },{ 
                name: 'angular.ng-tagsinput', 
                url: '/ng-tagsinput',
                views:{
                    "content@": {
                        templateUrl: 'pages/angular/ng-tagsinput.html',
                        controller: 'tagsinputController as tagsinput'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('ng-tagsinput',{serie: true});
                    }]
                }
                },{ 
                name: 'angular.gridster', 
                url: '/gridster',
                views:{
                    "content@": {
                        templateUrl: 'pages/angular/gridster.html',
                        controller: 'gridsterController as gridsterjs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('gridster',{serie: true});
                    }]
                }
                },{ 
                name: 'angular.ui-cropper', 
                url: '/ui-cropper',
                views:{
                    "content@": {
                        templateUrl: 'pages/angular/ui-cropper.html',
                        controller: 'uicropperController as uicropper'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load(['ui-cropper','downloadjs'],{serie: true});
                    }]
                }
                },{ 
                name: 'angular.ws-table', 
                url: '/ws-table',
                views:{
                    "content@": {
                        templateUrl: 'pages/angular/ws-table.html',
                        controller: 'wstableController as wstable'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('ws-table');
                    }]
                }
                },{ 
                name: 'angular.colorpicker', 
                url: '/colorpicker',
                views:{
                    "content@": {
                        templateUrl: 'pages/angular/colorpicker.html',
                        controller: 'colorpickerController as colorpickerjs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('colorpicker',{serie: true});
                    }]
                }
                },{ 
                name: 'angular.typewrite', 
                url: '/typewrite',
                views:{
                    "content@": {
                        templateUrl: 'pages/angular/typewrite.html',
                        controller: 'typewriteController as typewritejs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('typewrite',{serie: true});
                    }]
                }
                },{ 
                name: 'angular.ws-chat', 
                url: '/ws-chat',
                views:{
                    "content@": {
                        templateUrl: 'pages/angular/ws-chat.html',
                        controller: 'wschatController as wschat'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('firebase');
                    }]
                }                    
                },{ 
                // State list of JQuery module
                name: 'jquery', 
                url: '/jquery', 
                views:{
                    "content": {
                        templateUrl: 'pages/jquery/jquery.html',
                        controller: 'jqueryjsController as jqueryjs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('module-jquery');
                    }]
                }                    
                },{ 
                name: 'jquery.download', 
                url: '/download',
                views:{
                    "content@": {
                        templateUrl: 'pages/jquery/download.html',
                        controller: 'downloadController as downloadjs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('downloadjs');
                    }]
                }
                },{ 
                name: 'jquery.gifplayer', 
                url: '/gifplayer',
                views:{
                    "content@": {
                        templateUrl: 'pages/jquery/gifplayer.html',
                        controller: 'gifplayerController as gifplayerjs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('gifplayer',{serie:true});
                    }]
                }
                },{ 
                name: 'jquery.highlight', 
                url: '/highlight',
                views:{
                    "content@": {
                        templateUrl: 'pages/jquery/highlight.html',
                        controller: 'highlightController as highlightjs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('highlight',{serie:true});
                    }]
                }
                },{ 
                name: 'jquery.bxslider-4', 
                url: '/bxslider-4',
                views:{
                    "content@": {
                        templateUrl: 'pages/jquery/bxslider.html',
                        controller: 'bxsliderjsController as bxsliderjs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('bxsliderjs',{serie:true});
                    }]
                }
                },{ 
                name: 'jquery.chart', 
                url: '/chart',
                views:{
                    "content@": {
                        templateUrl: 'pages/jquery/chart.html',
                        controller: 'chartjsController as chartjs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('chartjs');
                    }]
                }
                },{ 
                name: 'jquery.caman', 
                url: '/caman',
                views:{
                    "content@": {
                        templateUrl: 'pages/jquery/caman.html',
                        controller: 'camanjsController as camanjs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load(['camanjs','downloadjs']);
                    }]
                }
                },{ 
                name: 'jquery.tilt', 
                url: '/tilt',
                views:{
                    "content@": {
                        templateUrl: 'pages/jquery/tilt.html',
                        controller: 'tiltjsController as tiltjs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load(['tiltjs','gifplayer'],{serie:true});
                    }]
                }
                },{ 
                name: 'jquery.slider-pips', 
                url: '/slider-pips',
                views:{
                    "content@": {
                        templateUrl: 'pages/jquery/slider-pips.html',
                        controller: 'sliderPipsController as sliderPips'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load(['slider-pips','gifplayer'],{serie:true});
                    }]
                }
                },{ 
                name: 'jquery.defiantjs', 
                url: '/defiantjs',
                views:{
                    "content@": {
                        templateUrl: 'pages/jquery/defiantjs.html',
                        controller: 'defiantjsController as defiantjs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('defiantjs');
                    }]
                }
                },{ 
                name: 'jquery.html2canvas', 
                url: '/html2canvas',
                views:{
                    "content@": {
                        templateUrl: 'pages/jquery/html2canvas.html',
                        controller: 'html2canvasController as html2canvasjs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('html2canvas', {serie:true});
                    }]
                }    
                },{ 
                name: 'jquery.isotope', 
                url: '/isotope',
                views:{
                    "content@": {
                        templateUrl: 'pages/jquery/isotope.html',
                        controller: 'isotopeController as isotopejs'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('isotope');
                    }]
                }
                },{ 
                name: 'jquery.jssocials', 
                url: '/jssocials',
                views:{
                    "content@": {
                        templateUrl: 'pages/jquery/jssocials.html',
                        controller: 'jssocialswsController as jssocialsws'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('jssocials', {serie:true});
                    }]
                }
                },{
                // State list of Bootstrap module
                name: 'bootstrap', 
                url: '/bootstrap', 
                views:{
                    "content": {
                        templateUrl: 'pages/bootstrap/bootstrap.html',
                        controller: 'bootstrapController as bootstrap'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('module-bootstrap');
                    }]
                }                    
                },{ 
                name: 'bootstrap.media', 
                url: '/media',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/media.html',
                        controller: 'mediaController as media'
                        }
                    }
                },{ 
                name: 'bootstrap.typography', 
                url: '/typography',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/typography.html',
                        controller: 'typographyController as typography'
                        }
                    }
                },{ 
                name: 'bootstrap.tables', 
                url: '/tables',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/tables.html',
                        controller: 'tablesController as tables'
                        }
                    }
                },{ 
                name: 'bootstrap.alerts', 
                url: '/alerts',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/alerts.html',
                        controller: 'alertsController as alerts'
                        }
                    }
                },{ 
                name: 'bootstrap.buttons', 
                url: '/buttons',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/buttons.html',
                        controller: 'buttonsController as buttons'
                        }
                    }
                },{ 
                name: 'bootstrap.cards', 
                url: '/cards',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/cards.html',
                        controller: 'cardsController as cards'
                        }
                    }
                },{ 
                name: 'bootstrap.sliders', 
                url: '/sliders',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/sliders.html',
                        controller: 'slidersController as sliders'
                        }
                    }
                },{ 
                name: 'bootstrap.spoilers', 
                url: '/spoilers',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/spoilers.html',
                        controller: 'spoilersController as spoilers'
                        }
                    }
                },{ 
                name: 'bootstrap.forms', 
                url: '/forms',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/forms.html',
                        controller: 'formsController as forms'
                        }
                    }
                },{ 
                name: 'bootstrap.views', 
                url: '/views',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/views.html',
                        controller: 'viewsController as views'
                        }
                    }
                },{ 
                name: 'bootstrap.listgroup', 
                url: '/listgroup',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/listgroup.html',
                        controller: 'listgroupController as listgroup'
                        }
                    }
                },{ 
                name: 'bootstrap.modals', 
                url: '/modals',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/modals.html',
                        controller: 'modalsController as modals'
                        }
                    }
                },{ 
                name: 'bootstrap.tabs', 
                url: '/tabs',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/tabs.html',
                        controller: 'tabsController as tabs'
                        }
                    }
                },{ 
                name: 'bootstrap.navigation', 
                url: '/navigation',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/navigation.html',
                        controller: 'navigationController as navigation'
                        }
                    }
                },{ 
                name: 'bootstrap.tooltips', 
                url: '/tooltips',
                views:{
                    "content@": {
                        templateUrl: 'pages/bootstrap/tooltips.html',
                        controller: 'tooltipsController as tooltips'
                        }
                    }
                },{
                // State list of Software module
                name: 'software', 
                url: '/software', 
                views:{
                    "content": {
                        templateUrl: 'pages/software/software.html',
                        controller: 'softwareController as software'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('module-software');
                    }]
                }                    
                },{ 
                name: 'software.git', 
                url: '/git',
                views:{
                    "content@": {
                        templateUrl: 'pages/software/git.html',
                        controller: 'gitController as git'
                        }
                    },
                },{ 
                name: 'software.sublimetext', 
                url: '/sublimetext',
                views:{
                    "content@": {
                        templateUrl: 'pages/software/sublimetext.html',
                        controller: 'sublimetextController as sublimetext'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('gifplayer');
                    }]
                }
                },{ 
                name: 'software.openserver', 
                url: '/openserver',
                views:{
                    "content@": {
                        templateUrl: 'pages/software/openserver.html',
                        controller: 'openserverController as openserver'
                        }
                    },
                },{
                // State list of Home module
                name: 'home', 
                url: '/',
                views:{
                    "content": {
                        templateUrl: 'pages/home/home.html',
                        controller: 'homeController as home'
                        }
                    },
                resolve: {
                    getModules: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('module-home');
                    }]
                }                    
                },{ 
                name: 'home.front-end', 
                url: 'front-end',
                views:{
                    "modalContent@": {
                        templateUrl: 'pages/home/modal/front-end.html'
                        }
                    },
                },{ 
                name: 'home.landing-page', 
                url: 'landing-page',
                views:{
                    "modalContent@": {
                        templateUrl: 'pages/home/modal/landing-page.html'
                        }
                    },
                },{ 
                name: 'home.wordpress', 
                url: 'wordpress',
                views:{
                    "modalContent@": {
                        templateUrl: 'pages/home/modal/wordpress.html'
                        }
                    },
                },{ 
                name: 'price', 
                url: '/price',
                views:{
                    "content": {
                        templateUrl: 'pages/home/price.html',
                        }
                    },
                },{ 
                name: 'examples', 
                url: '/examples',
                views:{
                    "content": {
                        templateUrl: 'pages/home/examples.html',
                        }
                    },
                }] 

                return states;        
            };
    })

})();

