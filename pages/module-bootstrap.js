
(function () {
    'use strict';
    angular
    .module('webspace.bootstrap',['ui.router'])
    .config(bootstrapConfig)
    .controller('bootstrapController', bootstrapController)
    .controller('mediaController', mediaController)
    .controller('typographyController', typographyController)
    .controller('tablesController', tablesController)
    .controller('alertsController', alertsController)
    .controller('buttonsController', buttonsController)
    .controller('cardsController', buttonsController)
    .controller('slidersController', slidersController)
    .controller('spoilersController', spoilersController)
    .controller('formsController', formsController)
    .controller('viewsController', viewsController)
    .controller('listgroupController', listgroupController)
    .controller('modalsController', modalsController)
    .controller('tabsController', tabsController)
    .controller('navigationController', navigationController)
    .controller('tooltipsController', tooltipsController)

/*===================================================
=            Bootstrap module controller            =
===================================================*/

    bootstrapController.$inject = [];
    function bootstrapController(){
        var bootstrap = this;

        // Отключение дергания к пустому якорю
        bootstrap.preventDefault = function (e) {
            e.preventDefault();        
        };

        angular.element('[data-toggle="popover"]').popover();
        angular.element('[data-toggle="tooltip"]').tooltip();

    }

/*=====  End of Bootstrap module controller  ======*/

/*========================================================
=            media configuration / controller            =
========================================================*/

    mediaController.$inject = [];
    function mediaController(){
        var media = this;
    }

/*=====  End of media configuration / controller  ======*/

/*=============================================================
=            typography configuration / controller            =
=============================================================*/

    typographyController.$inject = [];
    function typographyController(){
        var typography = this;
    }

/*=====  End of typography configuration / controller  ======*/

/*==========================================================
=            tables configuration / controllers            =
==========================================================*/

    tablesController.$inject = [];
    function tablesController(){
        var tables = this;
    }

/*=====  End of tables configuration / controllers  ======*/

/*=========================================================
=            alerts configuration / controller            =
=========================================================*/

    alertsController.$inject = [];
    function alertsController(){
        var alerts = this;
    }

/*=====  End of alerts configuration / controller  ======*/

/*===========================================================
=            buttons configuration / controllers            =
===========================================================*/

    buttonsController.$inject = [];
    function buttonsController(){
        var buttons = this;
    }

/*=====  End of buttons configuration / controllers  ======*/

/*========================================================
=            cards configuration / controller            =
========================================================*/

    cardsController.$inject = [];
    function cardsController(){
        var cards = this;
    }

/*=====  End of cards configuration / controller  ======*/

/*==========================================================
=            sliders configuration / controller            =
==========================================================*/

    slidersController.$inject = [];
    function slidersController(){
        var sliders = this;
    }

/*=====  End of sliders configuration / controller  ======*/

/*===========================================================
=            spoilers configuration / controller            =
===========================================================*/

    spoilersController.$inject = [];
    function spoilersController(){
        var spoilers = this;
    }

/*=====  End of spoilers configuration / controller  ======*/

/*========================================================
=            forms configuration / controller            =
========================================================*/

    formsController.$inject = [];
    function formsController(){
        var forms = this;
    }

/*=====  End of forms configuration / controller  ======*/

/*========================================================
=            views configuration / controller            =
========================================================*/

    viewsController.$inject = [];
    function viewsController(){
        var views = this;
    }

/*=====  End of views configuration / controller  ======*/

/*============================================================
=            listgroup configuration / controller            =
============================================================*/

    listgroupController.$inject = [];
    function listgroupController(){
        var listgroup = this;
    }

/*=====  End of listgroup configuration / controller  ======*/

/*=========================================================
=            modals configuration / controller            =
=========================================================*/

    modalsController.$inject = [];
    function modalsController(){
        var modals = this;

        angular.element('.modal').on('show.bs.modal', function (e) {
            angular.element(e.target).appendTo("body");
        })
    }

/*=====  End of modals configuration / controller  ======*/

/*=======================================================
=            tabs configuration / controller            =
=======================================================*/

    tabsController.$inject = [];
    function tabsController(){
        var tabs = this;
    }

/*=====  End of tabs configuration / controller  ======*/

/*=============================================================
=            navigation configuration / controller            =
=============================================================*/

    navigationController.$inject = [];
    function navigationController(){
        var navigation = this;
    }

/*=====  End of navigation configuration / controller  ======*/

/*===========================================================
=            tooltips configuration / controller            =
===========================================================*/

    tooltipsController.$inject = [];
    function tooltipsController(){
        var tooltips = this;

        // Toggle tooltips and popovers
        angular.element('[data-toggle="tooltip"]').tooltip();      
        angular.element('[data-toggle="popover"]').popover();      
    }

/*=====  End of tooltips configuration / controller  ======*/

/*======================================================
=            Bootstrap module configuration            =
======================================================*/

    bootstrapConfig.$inject = ['$stateProvider'];
    function bootstrapConfig($stateProvider) {
        // TODO: Type something    
    }

/*=====  End of Bootstrap module configuration  ======*/

})();