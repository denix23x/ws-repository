(function () {
    'use strict';
    angular
    .module('webspace.software',['ui.router'])
    .config(softwareConfig)
    .controller('softwareController', softwareController)
    .controller('gitController', gitController)
    .controller('sublimetextController', sublimetextController)
    .controller('openserverController', openserverController)

/*==================================================
=            Software module controller            =
==================================================*/

    softwareController.$inject = [];
    function softwareController() {
        var software = this;
    }

/*=====  End of Software module controller  ======*/

/*======================================================
=            Git configuration / controller            =
======================================================*/

    gitController.$inject = [];
    function gitController() {
        var git = this;
    }

/*=====  End of Git configuration / controller  ======*/

/*===============================================================
=            Sublime Text configuration / controller            =
===============================================================*/

    sublimetextController.$inject = [];
    function sublimetextController() {
        var sublimetext = this;

        angular.element('.gifplayer').gifplayer({onPlay: function(){$('.gp-gif-element').addClass('img-thumbnail');}});
    }

/*=====  End of Sublime Text configuration / controller  ======*/

/*==============================================================
=            Open Server configuration / controller            =
==============================================================*/

    openserverController.$inject = [];
    function openserverController() {
        var openserver = this;
    }

/*=====  End of Open Server configuration / controller  ======*/


/*=====================================================
=            Software module configuration            =
=====================================================*/

    softwareConfig.$inject = ['$stateProvider'];
    function softwareConfig($stateProvider) {
        // TODO: Type something    
    }

/*=====  End of Software module configuration  ======*/

})();