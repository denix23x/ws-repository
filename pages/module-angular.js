(function () {
    'use strict';
    angular
    .module('webspace.angular',['ui.router'])
    .config(angularConfig)
    .controller('angularController', angularController)
    .controller('scrollbarsController', scrollbarsController)
    .controller('swangularController', swangularController)
    .controller('angucompleteController', angucompleteController)
    .controller('summernoteController', summernoteController)
    .controller('hotkeysController', hotkeysController)
    .controller('tagsinputController', tagsinputController)
    .controller('gridsterController', gridsterController)
    .controller('uicropperController', uicropperController)
    .controller('wstableController', wstableController)
    .controller('colorpickerController', colorpickerController)
    .controller('typewriteController', typewriteController)
    .controller('wschatController', wschatController)


/*===================================================
=            AngularJS module controller            =
===================================================*/

    angularController.$inject = [];
    function angularController() {
        var angularjs = this;
    };

/*=====  End of AngularJS module controller  ======*/

/*================================================================
=            ng-scrollbars configuration / controller            =
================================================================*/

    scrollbarsController.$inject = ['$location', '$http'];
    function scrollbarsController($location, $http) {
        var scrollbars = this,
            panels = [],
            defaultText = 'Est Schlitz shoreditch fashion axe. Messenger bag cupidatat Williamsburg sustainable aliqua, artisan duis pickled pitchfork. Semiotics Banksy ad roof party, jean shorts selvage mollit vero consectetur hashtag before they sold out blue bottle qui nihil aute. Aliquip artisan retro squid ullamco. Vegan enim Williamsburg, eu umami shabby chic single-origin coffee et.',
            themes = ['minimal', 'minimal-dark', 'light-2', 'dark-2', 'light-3', 'dark-3', 'light-thick', 'dark-thick', 'dark-thin', 'light-thin', 'rounded', 'rounded-dark', 'rounded-dots', 'rounded-dots-dark', '3d', '3d-dark', '3d-thick', '3d-thick-dark'];        
        
        for (var i = 0; i < themes.length; i++) {
            var scrollbarConfig = {
              advanced: {
                updateOnContentResize: true
              },
              scrollButtons: {
                  scrollAmount: 'auto', // scroll amount when button pressed
                  enable: true // enable scrolling buttons by default
              },
              axis: 'yx' // enable 2 axis scrollbars by default              
            }
            scrollbarConfig.theme = themes[i % themes.length];
            var text = defaultText;
            var title = scrollbarConfig.theme;
            var cssClasses = "";
            switch (scrollbarConfig.theme) {
              case 'dark':
              case 'dark-2':
              case 'dark-3':
              case 'dark-thick':
              case 'dark-thin':
              case 'rounded-dark':
              case 'minimal-dark':
              case 'rounded-dots-dark':
              case '3d-thick-dark':
              case '3d-dark':
                cssClasses = "light-background";
                break;
            }

            panels.push({title:title,text:text,config:scrollbarConfig,cssClasses:cssClasses});
        }
            scrollbars.panels = panels; 

/*----------  Individual scrollbars  ----------*/

            scrollbars.faded = {
            autoHideScrollbar: false,
            theme: 'dark',
            advanced:{
                updateOnContentResize: true
            },
                setHeight: 350,
                scrollInertia: 500,
                axis: 'y',
                      scrollButtons: {
                          scrollAmount: 'auto', // scroll amount when button pressed
                          enable: true // enable scrolling buttons by default
                      }
            }
            scrollbars.primary  = {
            autoHideScrollbar: false,
            theme: 'light-2',
            advanced:{
                updateOnContentResize: true
            },
                setHeight: 350,
                scrollInertia: 500,
                axis: 'yx',
                      scrollButtons: {
                          scrollAmount: 'auto', // scroll amount when button pressed
                          enable: true // enable scrolling buttons by default
                      }
            }
            scrollbars.images  = {
            autoHideScrollbar: false,
            theme: 'dark-thin',
            advanced:{
                updateOnContentResize: true
            },
                scrollInertia: 500,
                axis: 'x',
                      scrollButtons: {
                          scrollAmount: 'auto', // scroll amount when button pressed
                          enable: true // enable scrolling buttons by default
                      }
            }  

            scrollbars.heroesFile = {};
            scrollbars.imageUrl = $location.protocol() + '://' + $location.host() + '/json/superheroes.json';
            scrollbars.loadHeroesFile = function() {
                $http.get(scrollbars.imageUrl).then(function successCallback(response) {
                  scrollbars.heroesFile = response.data;
                }, function errorCallback(response) {});
            };

            scrollbars.themeSwitch = function (panel) {
              return panel.config.theme.indexOf('dark') !== -1;
            }

            scrollbars.switchReverse = true;
    };

/*=====  End of ng-scrollbars configuration / controller  ======*/

/*============================================================
=            swangular configuration / controller            =
============================================================*/

    swangularController.$inject = ['$interval'];
    function swangularController($interval) {
      var swangular = this;

      angular.element('.gifplayer').gifplayer({onPlay: function(){$('.gp-gif-element').addClass('img-thumbnail');}});
     
      swangular.swal = function (type) {
        switch(type) {
            case 'basic': swal('Any fool can use a computer')
                break;
            case 'text': swal('Swangular Demo', 'Great demo, right?', 'question');
                break;
            case 'success': swal('Good job!', 'You clicked the button!', 'success');
                break;
            case 'timer': swal({ 
                    title: 'Auto close alert!',
                    html: 'I will close in <span id="countDown">5</span> second<span id="damnCharacter">s</span>..',
                    timer: 5000,
                    onOpen: function () {
                      var sec = 5;
                      swangular.timer = $interval(function () {
                         sec--;
                         angular.element('#countDown').text(sec);
                         if (sec == 1) {
                          angular.element('#damnCharacter').remove();
                         }
                      },1000,5);
                    },                      
                  }).then(function () {
                      $interval.cancel(swangular.timer); // Dismiss by button - reset timer
                  },function () {
                      $interval.cancel(swangular.timer); // Dismiss by overlay - reset timer
                    }
                  )
                break;
            case 'html': swal({
                  title: '<i>HTML</i> <u>example</u>',
                  type: 'info',
                  html:
                    'You can use <b>bold text</b>, ' +
                    '<a href="//github.com">links</a> ' +
                    'and other HTML tags',
                  showCloseButton: true,
                  confirmButtonText: '<i class="fa fa-thumbs-up"></i> Great!'
                })
                break;     
            case 'animation': swal({
                  title: 'jQuery HTML example',
                  html: $('<div>')
                    .addClass('some-class')
                    .text('jQuery is everywhere.'),
                  animation: false,
                  customClass: 'animated tada'
                })
                break;
            case 'attached': swal({
                  title: 'Are you sure?',
                  text: "You won't be able to revert this!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, delete it!',
                  cancelButtonText: 'No, cancel!',
                  confirmButtonClass: 'btn btn-success mr-3',
                  cancelButtonClass: 'btn btn-danger',
                  buttonsStyling: false
                }).then(function () {
                  swal('Deleted!','Your file has been deleted.','success')
                }, function (dismiss) {
                  if (dismiss === 'cancel') {
                    swal('Cancelled','Your imaginary file is safe :)','error')
                  }
                })
                break;
            case 'background': swal({
                  title: '<span class="text-white">Custom width, padding, background-image.</span>',
                  width: 600,
                  padding: 100,
                  buttonsStyling: false,
                  showConfirmButton: true,
                  confirmButtonText: 'Okay!',
                  confirmButtonClass: 'btn btn-success',
                  background: '#fff url(../img/svg/background-night-desert.svg)',
                  customClass: 'swangular-bg-image'
                })
                break;
            case 'chaining': swal.setDefaults({
                  input: 'text',
                  confirmButtonText: 'Next &rarr;',
                  showCancelButton: true,
                  animation: false,
                  progressSteps: ['1', '2', '3']
                })
                var steps = [
                  {
                    title: 'Question 1',
                    text: 'Chaining swal2 modals is easy'
                  },
                  'Question 2',
                  'Question 3'
                ]
                swal.queue(steps).then(function (result) {
                  swal.resetDefaults()
                  swal({
                    title: 'All done!',
                    html:
                      'Your answers: <pre>' +
                        JSON.stringify(result) +
                      '</pre>',
                    confirmButtonText: 'Lovely!',
                    showCancelButton: false
                  })
                }, function () {
                  swal.resetDefaults()
                })
                break;
            case 'query': swal.queue([{
                  title: 'Your public IP',
                  confirmButtonText: 'Show my public IP',
                  text:
                    'Your public IP will be received ' +
                    'via AJAX request',
                  showLoaderOnConfirm: true,
                  preConfirm: function () {
                    return new Promise(function (resolve) {
                      $.get('https://api.ipify.org?format=json')
                        .done(function (data) {
                          swal.insertQueueStep(data.ip)
                          resolve()
                        })
                    })
                  }
                }])
                break;    
            case 'email': swal({
                  title: 'Input email address',
                  input: 'email'
                }).then(function (email) {
                  swal({ 
                    type: 'success',
                    html: 'Entered email: ' + email
                  })
                })
                break;                                                                                                                                                                                                                    
            default:
                // code block
        }
      };        
    };

/*=====  End of swangular configuration / controller  ======*/

/*===============================================================
=            Angucomplete configuration / controller            =
===============================================================*/

    angucompleteController.$inject = ['$location', '$http'];
    function angucompleteController($location, $http) {
        var angucomplete = this;

        angucomplete.countriesUrl = $location.protocol() + '://' + $location.host() + '/json/countries.json';

        angucomplete.loadCountries = function() {
            $http.get(angucomplete.countriesUrl).then(function successCallback(response) {
               angucomplete.countries = response.data;
            }, function errorCallback(response) {});
        };

        angucomplete.heroes = [
            {name: "Captain America", realname: "Steven Rogers", city: "New York", picture: "../img/png/superheroes/captainamerica.png"},
            {name: "Batman", realname: "Bruce Wayne", city: "Gotham City", picture: "../img/png/superheroes/batman.png"},
            {name: "Aquaman", realname: "Arthur Curry", city: "Atlantis", picture: "../img/png/superheroes/aquaman.png"},
            {name: "Captain Marvel", realname: "Billy Batson", city: "Fawcett City", picture: "../img/png/superheroes/captainmarvel.png"}
        ];

    };

/*=====  End of Angucomplete configuration / controller  ======*/

/*=============================================================
=            Summernote configuration / controller            =
=============================================================*/

    summernoteController.$inject = [];
    function summernoteController() {
        var summernotejs = this;

        summernotejs.summernoteOptions = {
          placeholder: 'Write you best story..',
          minHeight: 200,
          focus: false,
          airMode: false,
          toolbar: [
                  ['edit',['undo','redo']],
                  ['headline', ['style']],
                  ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
                  ['fontface', ['fontname']],
                  ['textsize', ['fontsize']],
                  ['fontclr', ['color']],
                  ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
                  ['height', ['height']],
                  ['table', ['table']],
                  ['insert', ['link','picture','video','hr']],
                  ['view', ['fullscreen', 'codeview']],
                  ['help', ['help']]
              ],
          dialogsInBody: true,
          dialogsFade: true,
          shortcuts: false
        };                

        summernotejs.init = function() { 
          angular.element('.popover').css('display', 'none');
        }

    };

/*=====  End of Summernote configuration / controller  ======*/

/*==========================================================
=            Hotkeys configuration / controller            =
==========================================================*/

    hotkeysController.$inject = ['hotkeys', '$scope'];
    function hotkeysController(hotkeys, $scope) {
        var hotkeysjs = this;

        hotkeys.add({
          combo: 'ctrl+x',
          callback: function() {
            angular.element('.hk-overlay').fadeIn(300);
            angular.element('body').css('overflow','hidden');
            $scope.$emit('overlay', true);
          }
        });

        hotkeys.add({
          combo: 'ctrl+alt+shift+x',
          callback: function() {
            angular.element('.hk-overlay').fadeIn(300);
            angular.element('body').css('overflow','hidden');
            $scope.$emit('overlay', true);
          }
        });

        hotkeys.add({
          combo: 'x',
          callback: function() {
            angular.element('.hk-overlay').fadeIn(300);
            angular.element('body').css('overflow','hidden');
            $scope.$emit('overlay', true);
          }
        });

        hotkeys.add({
          combo: 'ctrl+z',
          callback: function() {
            angular.element('.hk-overlay').fadeOut(300);
            angular.element('body').css('overflow','visible');
            $scope.$emit('overlay', false);
          }
        });    

        hotkeys.add({
          combo: 'esc',
          callback: function() {
            angular.element('.hk-overlay').fadeOut(300);
            angular.element('body').css('overflow','visible');
            $scope.$emit('overlay', false);
          }
        }); 

    };

/*=====  End of Hotkeys configuration / controller  ======*/

/*===============================================================
=            ng-tagsinput configuration / controller            =
===============================================================*/

    tagsinputController.$inject = ['$location', '$http'];
    function tagsinputController($location, $http) {
        var tagsinput = this;

        tagsinput.heroesFile = {};
        tagsinput.simpleTags = ['newmexico', 'rims', 'shoppingonline', 'hockey', 'traditionalart', 'htc', 'pinterest', 'españa'];
        tagsinput.numberTags = ['10', '2154', '30'];
        tagsinput.tags = ['Batman', 'Superman', 'Flash'];
        tagsinput.tagsUrl = $location.protocol() + '://' + $location.host() + '/json/superheroes.json';

        tagsinput.loadTags = function() {
            $http.get(tagsinput.tagsUrl).then(function successCallback(response) {
              tagsinput.heroesFile = response.data;
            }, function errorCallback(response) {});
        };

        tagsinput.searchTags = function($query) {
            return _.chain(tagsinput.heroesFile)
                .filter(function(x) { return !$query || x.name.toLowerCase().indexOf($query.toLowerCase()) > -1; })
                .sortBy('name')
                .value();
        }
    };

/*=====  End of ng-tagsinput configuration / controller  ======*/

/*===========================================================
=            Gridster configuration / controller            =
===========================================================*/

    gridsterController.$inject = ['$scope'];
    function gridsterController($scope) {
        var gridsterjs = this;

        gridsterjs.standardItems = [
          { sizeX: 2, sizeY: 1, row: 0, col: 0 },
          { sizeX: 2, sizeY: 2, row: 0, col: 2 },
          { sizeX: 1, sizeY: 1, row: 0, col: 4 },
          { sizeX: 1, sizeY: 1, row: 0, col: 5 },
          { sizeX: 2, sizeY: 1, row: 1, col: 0 },
          { sizeX: 1, sizeY: 1, row: 1, col: 4 },
          { sizeX: 1, sizeY: 2, row: 1, col: 5 },
          { sizeX: 1, sizeY: 1, row: 2, col: 0 },
          { sizeX: 2, sizeY: 1, row: 2, col: 1 },
          { sizeX: 1, sizeY: 1, row: 2, col: 3 },
          { sizeX: 1, sizeY: 1, row: 2, col: 4 }
        ];     

        gridsterjs.gridsterOpts = {
            columns: 6, // the width of the grid, in columns
            pushing: true, // whether to push other items out of the way on move or resize
            floating: true, // whether to automatically float items up so they stack (you can temporarily disable if you are adding unsorted items with ng-repeat)
            swapping: false, // whether or not to have items of the same size switch places instead of pushing down if they are the same size
            width: 'auto', // can be an integer or 'auto'. 'auto' scales gridster to be the full width of its containing element
            colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
            rowHeight: 'match', // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
            margins: [10, 10], // the pixel distance between each widget
            outerMargin: true, // whether margins apply to outer edges of the grid
            sparse: false, // "true" can increase performance of dragging and resizing for big grid (e.g. 20x50)
            isMobile: false, // stacks the grid items if true
            mobileBreakPoint: 600, // if the screen is not wider that this, remove the grid layout and stack the items
            mobileModeEnabled: false, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
            minColumns: 1, // the minimum columns the grid must have
            minRows: 2, // the minimum height of the grid, in rows
            maxRows: 100,
            defaultSizeX: 2, // the default width of a gridster item, if not specifed
            defaultSizeY: 1, // the default height of a gridster item, if not specified
            minSizeX: 1, // minimum column width of an item
            maxSizeX: null, // maximum column width of an item
            minSizeY: 1, // minumum row height of an item
            maxSizeY: null, // maximum row height of an item
            resizable: {
               enabled: true,
               handles: ['n', 'e', 's', 'w', 'ne', 'se', 'sw', 'nw'],
               start: function(event, $element, widget) {
                  $scope.$emit('overlay', true);
                },
               resize: function(event, $element, widget) {},
               stop: function(event, $element, widget) {
                  $scope.$emit('overlay', false);
                }
            },
            draggable: {
               enabled: true, // whether dragging items is supported
               // handle: '.my-class', // optional selector for drag handle
               start: function(event, $element, widget) {
                  $scope.$emit('overlay', true);
                }, 
               drag: function(event, $element, widget) {},
               stop: function(event, $element, widget) {
                  $scope.$emit('overlay', false);
                }
            }
        };

    };

/*=====  End of Gridster configuration / controller  ======*/

/*=============================================================
=            ui-cropper configuration / controller            =
=============================================================*/

    uicropperController.$inject = ['$scope', '$location'];
    function uicropperController($scope, $location) {
        var uicropper = this;

        uicropper.type = 'rectangle';
        uicropper.render = true;

        uicropper.switchType = function (type) {
          uicropper.render = !uicropper.render;
          switch(type) {
              case 'rectangle': uicropper.type = 'rectangle'
                  break;              
              case 'circle': uicropper.type = 'circle'
                  break;              
              case 'square': uicropper.type = 'square'
                  break;
              default:
                  // code block
              }
          return uicropper.render = !uicropper.render;
        }        

        uicropper.downloadCroppedImage = function () {
          download(uicropper.croppedImage, "cropped-image.png", "image/png");
        }

        uicropper.image = $location.protocol() + '://' + $location.host() + '/img/jpg/owl-image.jpg';
        uicropper.croppedImage = '';

        uicropper.rectangleWidth = 150;
        uicropper.rectangleHeight = 150;

        $scope.cropper = {
            cropWidth: uicropper.rectangleWidth,
            cropHeight: uicropper.rectangleHeight
        }

        angular.element('#cropInput').on('change', function (e) {
          var file = e.currentTarget.files[0],
              reader = new FileReader();

              reader.onload = function(e) {
                $scope.$apply(function($scope) {
                  uicropper.image = e.target.result;
                });
              };
          reader.readAsDataURL(file);     
        });        
    };

/*=====  End of ui-cropper configuration / controller  ======*/

/*===========================================================
=            ws-table configuration / controller            =
===========================================================*/

    wstableController.$inject = ['$location','$http'];
    function wstableController($location, $http) {
        var wstable = this;

        wstable.tableItemsUrl = $location.protocol() + '://' + $location.host() + '/json/people.json';
        wstable.currentPage = 1;
        wstable.pageSize = 10;      

        wstable.tableItemsLoad = function() {
            $http.get(wstable.tableItemsUrl).then(function successCallback(response) {
              wstable.tableItems = response.data;
            }, function errorCallback(response) {});
        };

        wstable.addTableRow = function () {
          wstable.tableItems.push({ 
            'index':wstable.tableItems.length + 1, 
            'name':wstable.tableName, 
            'surname': wstable.tableSurname, 
            'company':wstable.tableCompany, 
            'email':wstable.tableEmail, 
            'phone':wstable.tablePhone 
          });
            wstable.tableName='';
            wstable.tableSurname='';
            wstable.tableCompany='';
            wstable.tableEmail='';
            wstable.tablePhone='';
          };

        wstable.sort = function(keyname){
            wstable.sortKey = keyname;   //set the sortKey to the param passed
            wstable.reverse = !wstable.reverse; //if true make it false and vice versa

            if (wstable.reverse) {
              angular.element('#sortDown').remove();
              angular.element('#' + keyname).append('<i id="sortUp" class="fa fa-caret-up px-1" aria-hidden="true"></i>');
            } else {
              angular.element('#sortUp').remove();
              angular.element('#' + keyname).append('<i id="sortDown" class="fa fa-caret-down px-1" aria-hidden="true"></i>');
            }
        }

        wstable.removeTableRow = function (item) {
            var index = -1;   
            var array = eval(wstable.tableItems);
            for (var i = 0; i < array.length; i++) {
              if (array[i].index === item) {
                index = i; break;
              }
            }
            if (index === -1) {
              console.log("Something gone wrong, can't delete row");
            }
            wstable.tableItems.splice(index,1);    
          };          
    };

/*=====  End of ws-table configuration / controller  ======*/

/*==============================================================
=            colorpicker configuration / controller            =
==============================================================*/

    colorpickerController.$inject = [];
    function colorpickerController() {
        var colorpickerjs = this;

        colorpickerjs.colorpickerHex = '#5cb85c';
        colorpickerjs.colorpickerRgb = 'rgb(240,173,78)';
        colorpickerjs.colorpickerRgba = 'rgba(217,83,79,1)';     
    };

/*=====  End of colorpicker configuration / controller  ======*/

/*=============================================================
=            typewrite configuration / controller             =
=============================================================*/

    typewriteController.$inject = [];
    function typewriteController() {
        var typewritejs = this;

        typewritejs.typewrite = false;

        typewritejs.startTyping = function (e) {
          angular.element(e.target).fadeOut(300);
          typewritejs.typewrite = true;
        }        
    };

/*=====  End of typewrite configuration / controller  ======*/

/*==========================================================
=            ws-chat configuration / controller            =
==========================================================*/

    wschatController.$inject = ['commonService'];
    function wschatController(commonService) {
        var wschat = this, chatApp;

        wschat.render = function () {
            chatApp.database().ref('/wschat/messages/').once('value').then(function(snapshot) {
                wschat.message = '';
                angular.element('#messageInput').val('');
                angular.element('#messageList').html('');
                angular.forEach(snapshot.val(), function(index){
                    angular.element('#messageList').append('<p class="message-item mr-5">'+index.text+'</p>');
                });
                angular.element('.message-item:odd').css("background", "#faebd7").removeClass('mr-5').addClass('ml-5');
                angular.element('.ws-chat-list')[0].scrollTop = angular.element('#messageList').height();
            });
        }

        // App re-using
        if (!firebase.apps.length) {
            chatApp = firebase.initializeApp(commonService.firebaseConfig(), 'chat');
            console.log('app clear create chat app'); wschat.render();
        } else if (firebase.app('chat')) {
            firebase.app('chat').delete().then(function() {
                chatApp = firebase.initializeApp(commonService.firebaseConfig(), 'chat');
                console.log('was been chat, delete, create chat app'); wschat.render();
            });  
        } else {
            chatApp = firebase.initializeApp(commonService.firebaseConfig(), 'chat');
            console.log('nothing else, just create chat app'); wschat.render();
        }

        wschat.sendMessage = function () {
          if (wschat.message.length != '') {
            wschat.messages = chatApp.database().ref('/wschat/messages/');
            wschat.messages = wschat.messages.push();
            wschat.messages.set({
                text: wschat.message
            },wschat.render());
          }
        }

        wschat.removeAll = function () {
          wschat.allMessages = chatApp.database().ref('/wschat/messages/');
          wschat.allMessages.remove();
          wschat.render();
        }          

        angular.element(document).ready(function () {
            chatApp.database().ref('/wschat/messages/').on('child_added', function(data) {
              wschat.render();
            });  
        });

    };

/*=====  End of ws-chat configuration / controller  ======*/

/*======================================================
=            AngularJS module configuration            =
======================================================*/

    angularConfig.$inject = ['$stateProvider'];
    function angularConfig($stateProvider) {    
        // TODO: Type something
    }

/*=====  End of AngularJS module configuration  ======*/

})();