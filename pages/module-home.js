(function () {
    'use strict';
    angular
    .module('webspace.home',['ui.router'])
    .config(homeConfig)
    .controller('homeController', homeController)

/*=================================================
=            Landing module controller            =
=================================================*/

    homeController.$inject = ['$location', '$state'];
    function homeController($location, $state) {
        var home = this;

        angular.element(document).ready(function () {

            home.iframe = $location.search();

            // Remove iphone frame if innerframe = true
            if (home.iframe.innerframe) {
                angular.element('.iphone-background').remove();
            } 
                       
            if ($state.current.name.indexOf('home.') !== -1) {
                angular.element('#modalView').modal('show')
            }

        });

        home.loadIframe = function () {
            angular.element('.iphone-iframe').attr('src', '/?innerframe=true');
            angular.element('.cover-iframe').addClass('ws-hide');
            angular.element('.iphone-iframe').removeClass('ws-hide');
        }



        // Function to animate slider captions 
        home.animationSlider = function (elems) {
            // Cache the animationend event in a variable
            var animEndEv = 'webkitAnimationEnd animationend';
            
            elems.each(function () {
                var $this = $(this),
                    $animationType = $this.data('animation');

                $this.addClass($animationType).one(animEndEv, function () {
                    $this.removeClass($animationType);
                });
            });
        }
    
        // Variables on page load 
        var myCarousel = angular.element('#animCarousel'),
            firstAnimatingElems = myCarousel.find('.carousel-item:first').find("[data-animation ^= 'animated']");
            
        // Initialize carousel 
        myCarousel.carousel();

        // Animate captions in first slide on page load 
        home.animationSlider(firstAnimatingElems);

        // Pause carousel  
        myCarousel.carousel('pause');

        // Other slides to be animated on carousel slide event 
        angular.element(myCarousel).on('slide.bs.carousel', function (e) {
            var animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
            home.animationSlider(animatingElems);
        });  

    }

/*=====  End of Landing module controller  ======*/

/*====================================================
=            Landing module configuration            =
====================================================*/

     homeConfig.$inject = ['$stateProvider'];
     function homeConfig($stateProvider) {
        // TODO: Type something
     }

/*=====  End of Landing module configuration  ======*/

})();