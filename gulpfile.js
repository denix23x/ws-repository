
// 	Dependencies declaration

var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	pump = require('pump'),
	concat = require('gulp-concat'),
	strip = require('gulp-strip-comments'),
 	cleanCSS = require('gulp-clean-css');

	// 	Build task declaration
	// 	
	//  get-components
	//  get-app
	//  get-utilities
	//  get-bootstrap
	//  get-angular
	//  get-css

	var paths = {
		getApp:['js/app.js', 'service/*.js'],
		getComponents:['template/*.js']
	};	

	gulp.task('watcher',function(){
		gulp.watch(paths.getApp, ['get-app']);
		gulp.watch(paths.getComponents, ['get-components']);
	});

	/*===========================================
	=            Compress components            =
	===========================================*/
	
	gulp.task('compress-components', function (cb) {
		pump ([gulp.src(paths.getComponents),uglify(),gulp.dest('temp/components')],cb);
	});

	gulp.task('get-components', ['compress-components'], function() {
		return gulp.src('temp/components/*.js')
			.pipe(concat('ws-components.min.js'))
			.pipe(gulp.dest('dist'));
	});	
	
	/*=====  End of Compress components  ======*/
	
	/*=================================================
	=            Compress app and services            =
	=================================================*/
	
	gulp.task('compress-app', function (cb) {
		pump ([gulp.src(['js/app.js', 'service/common-service.js']),uglify(),gulp.dest('temp/app')],cb);
	});	

	gulp.task('get-app', ['compress-app'], function() {
		return gulp.src('temp/app/*.js')
			.pipe(concat('ws-app.min.js'))
			.pipe(gulp.dest('dist'));
	});		
	
	/*=====  End of Compress app and services  ======*/
	
	/*======================================================
	=            Concat loaders and utilities              =
	======================================================*/

	// Strip developers comments
	gulp.task('strip-utilities', function () {
		return gulp.src(['js/app-base/less.min.js', 'js/strictly-angular/ocLazyLoad.min.js', 'js/strictly-angular/angular-state-loader.min.js', 'js/strictly-jquery/html2canvas.min.js', 'js/strictly-jquery/jspdf.min.js'])
			.pipe(strip())
			.pipe(gulp.dest('temp/utilities'));
	});
	
	gulp.task('get-utilities', ['strip-utilities'], function() {
		return gulp.src('temp/utilities/*.js')
			.pipe(concat('ws-utilities.min.js'))
			.pipe(gulp.dest('dist'));
	});		
	
	/*======  End of Concat loaders and utilities  =======*/

	/*==========================================
	=            Concat Bootstrap 4            =
	==========================================*/
	
	// Strip developers comments
	gulp.task('strip-bootstrap', function () {
		return gulp.src(['js/app-base/jquery-3.1.1.min.js', 'js/app-base/tether.min.js', 'js/app-base/bootstrap.min.js'])
			.pipe(strip())
			.pipe(gulp.dest('temp/bootstrap'));
	});
	
	// By order
	gulp.task('get-bootstrap', ['strip-bootstrap'], function() {
		return gulp.src(['temp/bootstrap/jquery-3.1.1.min.js', 'temp/bootstrap/tether.min.js', 'temp/bootstrap/bootstrap.min.js'])
			.pipe(concat('ws-bootstrap.min.js'))
			.pipe(gulp.dest('dist'));
	});	
	
	/*=====  End of Concat Bootstrap 4  ======*/

	/*========================================
	=            Concat AngularJS            =
	========================================*/

	// Strip developers comments
	gulp.task('strip-angular', function () {
		return gulp.src(['js/app-base/angular.min.js', 'js/app-base/angular-ui-router.min.js', 'js/app-base/angular-animate.min.js', 'js/app-base/angular-touch.min.js'])
			.pipe(strip())
			.pipe(gulp.dest('temp/angular'));
	});
	
	// By order
	gulp.task('get-angular', ['strip-angular'], function() {
		return gulp.src(['js/app-base/angular.min.js', 'js/app-base/angular-ui-router.min.js', 'js/app-base/angular-animate.min.js', 'js/app-base/angular-touch.min.js'])
			.pipe(concat('ws-angular.min.js'))
			.pipe(gulp.dest('dist'));
	});		
	
	/*=====  End of Concat AngularJS  ======*/

	/*=============================================
	=            Minify CSS and concat            =
	=============================================*/

	gulp.task('minify-css', function () {
		return gulp.src('css/app-base/*.css')
			.pipe(cleanCSS())
			.pipe(gulp.dest('temp/css'));
	});		

	gulp.task('get-css', ['minify-css'], function() {
		return gulp.src('temp/css/*.css')
			.pipe(concat('ws-style.min.css'))
			.pipe(gulp.dest('dist'));
	});	

	/*=====  End of Minify CSS and concat  ======*/












