(function () {
    'use strict';
    angular.module('webspace',[
        'ui.router',
        'ngAnimate',
        'ngTouch',
        'oc.lazyLoad',
        'ec.stateloader'
    ])
    .config(mainConfig)
    .run(mainRun)
    .controller('mainController', mainController)

/*==============================================
=            Main app configuration            =
==============================================*/

    mainConfig.$inject = ['$urlRouterProvider', '$locationProvider', 'commonServiceProvider', '$ocLazyLoadProvider', '$stateProvider'];
    function mainConfig($urlRouterProvider, $locationProvider, commonServiceProvider, $ocLazyLoadProvider, $stateProvider) {
        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode({
          enabled: true,
          requireBase: true
        });

        $ocLazyLoadProvider.config(commonServiceProvider.$get().getPath());
      
        commonServiceProvider.$get().getState().forEach(function(state) {
          $stateProvider.state(state);
        });

        console.log(commonServiceProvider.$get().displaySize());
    }

/*=====  End of Main app configuration  ======*/

/*==========================================
=            Main app run event            =
==========================================*/

    mainRun.$inject = ['$rootScope'];
    function mainRun ($rootScope) {
        var run = this,
            modalStates = ['home.front-end', 'home.landing-page', 'home.wordpress'];

        // ScrollTop on all pages except for modal states
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
            var modal = false;

            angular.forEach(modalStates, function(state) {
                if ((state == toState.name) || (state == fromState.name)) {modal = true;}
            }, modal ? false : angular.element("html, body").animate({scrollTop: 0}, 600));            

        })
    }

/*=====  End of Main app run event  ======*/

/*===========================================
=            Main app controller            =
===========================================*/

    //  TRUE = only if have html directive: ng-controller="mainController as main" - in index.php

    mainController.$inject = [];
    function mainController() {
        var main = this;
        // TODO : Type something :)
    }

/*=====  End of Main app controller  ======*/

/*=============================================================================
=            Directive to get DOM ready, and set preloader = false            =
=============================================================================*/

    angular.module('webspace').directive('domLoad',['$document', function($document) {
        function link(scope , element , attrs) {
            element.ready(function () {
                // After main controller was initialized
            });
            scope.$on('$viewContentLoaded',function() {
                var preloader = angular.element('.preloader');
                
                    preloader.fadeOut(300,function () {
                        angular.element('.wrapper').removeClass('invisible');
                        preloader.remove();
                });
                // console.log(" ===> Called on View Load ");
            });
        }
        return {link: link};
        }
    ]);

/*=====  End of Directive to get DOM ready, and set preloader = false  ======*/

})();