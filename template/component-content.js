(function () {
    'use strict';
    angular
    .module('webspace').component('webspaceContent', {
        templateUrl: 'template/layout/content.html',
        controller: ('contentController', contentController),
        controllerAs: 'content'
    })

    // Start content component

    contentController.$inject = ['$location', '$rootScope', '$state', '$scope'];
    function contentController($location, $rootScope, $state, $scope) {
    	var content = this;

/*===========================================
=            breadcrumbs handler            =
===========================================*/

		$rootScope.$on('$stateChangeSuccess', 
		function(event, toState, toParams, fromState, fromParams){
			content.breadcrumbs = [];

			if ($location.url() == '/') {
				content.breadcrumbs[0] = 'home';
			} else {
				content.breadcrumbs = $location.url().split("/");
				content.breadcrumbs[0] = 'home';
			}
		})

/*=====  End of breadcrumbs handler  ======*/

/*===================================================
=            html2canvas render function            =
===================================================*/

        content.renderSection = function (task) {
            var w = angular.element('#content > div > section').width(),
                h = null,
                code = null;

            angular.element('section#content > div > section').appendTo(document.body);
            angular.element('body > section').clone().appendTo('#content > div').attr("id","sectionClone");
            angular.element('body > section').width(w).children().addClass('spec-class-for-h2c');

            // Code element handler
            if (angular.element('body > section code').length > 0) {
                angular.forEach(angular.element('body > section code'), function(index){
                    angular.element(index).addClass('spec-class-for-h2c');
                });   
                code = true;
            }

            var taskHTML2Canvas = new Promise(function (resolve, reject) {
                html2canvas(angular.element("body > section").get(0), {              
                        allowTaint: true,
                        useCORS: true,          
                    onrendered: function (canvas) { 
                        h = canvas.height;
                        resolve(canvas);
                    }
                });
            });

            Promise.all([taskHTML2Canvas]).then(function (canvas) {
                angular.element('section#sectionClone').remove();
                angular.element('body > section').appendTo('#content > div');
                angular.element('#content > div > section').children().removeClass('spec-class-for-h2c').removeAttr('style');

                // Code element handler
                if (code) {
                    angular.forEach(angular.element('#content > div > section code'), function(index){
                        angular.element(index).removeClass('spec-class-for-h2c');
                    });   
                    code = false;
                }               

                switch(task) {
                    case 'save': content.savePDF(canvas, h);
                        break;
                    case 'print': content.printPage(canvas);
                        break;               
                    default:
                        // TODO: type something default code block    
                    }
            });
        }

/*=====  End of html2canvas render function  ======*/

/*===========================================================
=            content print + savePDF + favorites            =
===========================================================*/
    
        content.savePDF = function (render, renderHeight) {
            var doc = new jsPDF(),
                pageHeight = doc.internal.pageSize.height;

            var setMark = function () {
                doc.setFontSize(24);
                doc.setTextColor(150);
                doc.text(105, Math.round(pageHeight /2), $location.absUrl(), null, 10, 'center');
            };

            doc.addImage(render[0].toDataURL(), 'JPEG', 15, 15, 180, 0);
            setMark();

            if (renderHeight > pageHeight) {
                var i = 1,
                    shift = pageHeight;

                while (i < Math.round(renderHeight / (pageHeight * 4))) {
                    doc.addPage();
                    doc.addImage(render[0].toDataURL(), 'JPEG',  15, -shift, 180, 0);
                    i++;
                    shift = shift + shift;
                    setMark();
                }
            }         

            doc.save('a4.pdf');
        };

        content.printPage = function (render) {
            var printWindow=window.open();

            printWindow.document.open();
            printWindow.document.write('<html><body onload="window.print()"><img src="'+render[0].toDataURL()+'" alt="Rendered Image"/></body></html>');
            printWindow.document.close();
    
            setTimeout(function(){printWindow.close();},10);
        }

/*=====  End of content print + savePDF + favorites  ======*/

        content.scrollTop = function () {
            angular.element("html, body").animate({scrollTop: 0}, 600);
            return false;            
        }

        // Return to parent state after close modal
        angular.element('#modalView').on('hidden.bs.modal', function (e) {
            $state.go('^');
            $scope.$emit('overlay', false);
        })

        // Get and Set modal title from call button
        angular.element('#modalView').on('show.bs.modal', function (e) {
            angular.element(this).find('.modal-title').text($(e.relatedTarget).data('title'));
            $scope.$emit('overlay', true);
        })

        // Toggle tooltips
		angular.element('[data-toggle="tooltip"]').tooltip();

    }

})();