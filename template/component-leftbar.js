(function () {
    'use strict';
    angular
    .module('webspace').component('webspaceLeftbar', {
        templateUrl: 'template/layout/leftbar.html',
        controller: ('leftbarController', leftbarController),
        controllerAs: 'leftbar'
    })

    // Start leftbar component

    leftbarController.$inject = ['$parse', '$swipe', '$rootScope'];
    function leftbarController($parse, $swipe, $rootScope) {
    	var leftbar = this,
            swipeDirection, screenOverlay;

            leftbar.toggleStatus = false;

/*===============================================================
=            leftbar collapse > localstorage handler            =
===============================================================*/

        leftbar.collapsed = [];

        leftbar.collapse = function (e) {
            var target = angular.element(e.target).data('target'),
                overlap = leftbar.collapsed.indexOf(target);

            if (overlap != '-1') {
                leftbar.collapsed.splice(overlap,1);
            }  else {
                leftbar.collapsed.push(target);
            }
        }

        angular.element(window).on('beforeunload', function () {
            localStorage.setItem('ws-leftbar', JSON.stringify(leftbar.collapsed));
        });

        leftbar.init = function () {
            if (localStorage.getItem('ws-leftbar') !== null) {
                var collapsed = JSON.parse(localStorage.getItem('ws-leftbar'));
                angular.forEach (collapsed, function(e) {
                    angular.element(e).toggleClass('show');
                });
                leftbar.collapsed = collapsed;
            }                  

        };

/*=====  End of leftbar collapse > localstorage handler  ======*/

/*========================================================
=            leftbar swipe binding and events            =
========================================================*/

        // Getting screenOverlay status

        $rootScope.$on('overlay', function(e, status) {
            screenOverlay = status;
        });

        // Binding swipe event on elements

        $swipe.bind(angular.element('[data-swipe="true"]'), {
            start: function(e){
                swipeDirection = e.x;
            },
            end: function(e){
                if (e.x == swipeDirection) {
                    return false;
                } else {
                    if (e.x > swipeDirection && Math.abs(e.x - swipeDirection) > 150) {
                        leftbar.swipeLeftbar('right');
                    } else if (e.x < swipeDirection && Math.abs(e.x - swipeDirection) > 150) {
                        leftbar.swipeLeftbar('left');
                    }            
                }
                // console.log(Math.abs(e.x - swipeDirection));
            }
        });

        // Launch swipeleftbar 

        leftbar.swipeLeftbar = function (d) {
            if (screenOverlay || window.getSelection().toString()) {
                return false;
            } else {
                var toggle = angular.element('.wrapper').hasClass('toggled');
                if (window.innerWidth < 768) {
                        if (((d == 'left') && (toggle == false)) || ((d == 'right') && (toggle))) {
                            return false;
                        } else {
                            angular.element('.wrapper').toggleClass('toggled');
                        }
                    } else {
                        if (((d == 'left') && (toggle)) || ((d == 'right') && (toggle == false))) {
                            return false;
                        } else {
                            angular.element('.wrapper').toggleClass('toggled');
                        }
                    }    
            }            
        }

/*=====  End of leftbar swipe binding and events  ======*/

        leftbar.toggleLeftbar = function () {
            leftbar.toggleStatus = !leftbar.toggleStatus;

            if (!leftbar.toggleStatus) {
                angular.element('.leftbar-wrapper').removeClass('bg-primary');     
            } else {
                angular.element('.leftbar-wrapper').addClass('bg-primary');       
            }
        }

        // Hide leftbar on orientation change?
        
        angular.element(window).on('orientationchange', function () {
            angular.element('.wrapper').removeClass('toggled');
        });

    }

})();