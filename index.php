<!DOCTYPE html>
<html lang="en">
<head>
    <base href="/">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title ng-bind-template="">Front-end developer</title>
    <!-- Bootstrap CSS and other-->
    <link rel="stylesheet" type="text/css" href="dist/ws-style.min.css">
    <link rel="stylesheet/less" type="text/css" href="css/style.less">
</head>

<body ng-app="webspace">   

    <div class="preloader">
        <span>Loading..</span>
    </div>   

    <div class="wrapper invisible">
        <div class="leftbar-wrapper" data-swipe="true">
            <webspace-leftbar><webspace-leftbar>
        </div>
        <div class="page-content-wrapper" data-swipe="true">
        <!-- <div class="page-content-wrapper animated fadeInLeft" data-swipe="true"> -->
            <div class="container-fluid">
                <div class="row px-4">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-6 pt-3">
                        <webspace-content></webspace-content>
                    </div> 
                    <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-2 pt-3">
                        <webspace-rightbar></webspace-rightbar>
                    </div>                 
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap 4 and his dependencies -->
    <script src="dist/ws-bootstrap.min.js"></script>    
    <!-- AngularJS and his dependencies -->
    <script src="dist/ws-angular.min.js"></script>    
    <!-- Webspace appplication -->
    <script src="dist/ws-app.min.js"></script>    
    <script src="dist/ws-components.min.js"></script>
    <script src="dist/ws-utilities.min.js"></script>
   
    <dom-load></dom-load>

</body>

</html>
